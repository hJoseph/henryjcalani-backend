package com.dh.fullstack.users.api.model.system;

import com.dh.fullstack.users.api.model.Gender;

import java.io.Serializable;

/**
 * @Autor Henry Joseph Calani A.
 **/
public interface AccountPerson extends Serializable {

    Long getPersonId();

    String getEmail();

    String getFirsName();

    String getLastName();

    Boolean getIsDeleted();

    Gender getGenderType();
}
