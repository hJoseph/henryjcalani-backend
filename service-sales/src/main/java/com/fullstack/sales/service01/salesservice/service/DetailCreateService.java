package com.fullstack.sales.service01.salesservice.service;

import com.fullstack.sales.service01.salesservice.input.ClientInput;
import com.fullstack.sales.service01.salesservice.input.DetailInput;
import com.fullstack.sales.service01.salesservice.model.domain.Client;
import com.fullstack.sales.service01.salesservice.model.domain.Detail;
import com.fullstack.sales.service01.salesservice.model.repositories.ClientRepository;
import com.fullstack.sales.service01.salesservice.model.repositories.DetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class DetailCreateService {
    private DetailInput detailInput;

    @Autowired
    private DetailRepository detailRepository;

    public Detail save(){

        return detailRepository.save(composeDetailInstance());
    }

    private Detail composeDetailInstance() {
        Detail instance = new Detail();
        instance.setTotalPrice(detailInput.getTotalPrice());
        instance.setTotalProducts(detailInput.getTotalProducts());
        return  instance;
    }

    public DetailInput getDetailInput() {
        return detailInput;
    }

    public void setDetailInput(DetailInput detailInput) {
        this.detailInput = detailInput;
    }
}
