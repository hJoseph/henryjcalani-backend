package com.fullstack.sales.service01.salesservice.model.domain;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Data
@Entity
@Table(name = "Client_table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "clientid", referencedColumnName = "personid")
})
public class Client extends Person{

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastPurchase", nullable = false)
    private Date lastPurchase;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender", length = 20, nullable = false)
    private Gender gender;


}
