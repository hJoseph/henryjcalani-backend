package com.fullstack.sales.service01.salesservice.controller;

import com.fullstack.sales.service01.salesservice.input.ClientInput;
import com.fullstack.sales.service01.salesservice.model.domain.Client;
import com.fullstack.sales.service01.salesservice.model.domain.Gender;
import com.fullstack.sales.service01.salesservice.model.domain.Person;
import com.fullstack.sales.service01.salesservice.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Api(
        tags = "Person rest",
        description = "Operations over Person"
)
@RestController
@RequestMapping("/client")
@RequestScope
public class PersonController {

    @Autowired
    private DeletePersonService deletePersonService;


    @ApiOperation(
            value = "Endpoint to delete client"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to delete client"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Not fount test"
            )
    })

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Person deleteClient(@PathVariable Long id){
        deletePersonService.setIdPerson(id);
        deletePersonService.execute();
        return deletePersonService.getPerson();
    }




}
