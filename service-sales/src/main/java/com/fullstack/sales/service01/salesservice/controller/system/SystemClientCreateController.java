package com.fullstack.sales.service01.salesservice.controller.system;

import com.fullstack.sales.service01.salesservice.command.SystemClientCreateCmd;
import com.fullstack.sales.service01.salesservice.controller.Constants;
import com.fullstack.sales.service01.salesservice.input.ClientInput;
import com.fullstack.sales.service01.salesservice.model.domain.Client;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @Autor Henry Joseph Calani A.
 **/

@Api(
        tags = Constants.ContactTag.NAME,
        description = Constants.ContactTag.DESCRIPTION
)
@RestController
@RequestMapping(Constants.BasePath.SYSTEM_CONTACTS)
@RequestScope
public class SystemClientCreateController {

    @Autowired
    private SystemClientCreateCmd systemClientCreateCmd;

    @ApiOperation(
            value = "Create a Client"
    )

    @RequestMapping(method = RequestMethod.POST)
    public Client createClient(@RequestBody ClientInput input) {
        systemClientCreateCmd.setClientInput(input);
        systemClientCreateCmd.execute();
        return systemClientCreateCmd.getClient();

    }
}
