package com.fullstack.sales.service01.salesservice.controller;

import com.fullstack.sales.service01.salesservice.input.EmployeeInput;
import com.fullstack.sales.service01.salesservice.model.domain.Employee;
import com.fullstack.sales.service01.salesservice.service.EmployeeCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Api(
        tags = "Employee rest",
        description = "Operations over Employee"
)
@RestController
@RequestMapping("/Employee")
@RequestScope
public class EmployeeController {

    @Autowired
    private EmployeeCreateService employeeCreateService;

    @ApiOperation(
            value = "Endpoint to create Employee"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to create client"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Not fount test"
            )
    })

    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeInput input) {
        employeeCreateService.setEmployeeInput(input);
        return employeeCreateService.save();
    }




}
