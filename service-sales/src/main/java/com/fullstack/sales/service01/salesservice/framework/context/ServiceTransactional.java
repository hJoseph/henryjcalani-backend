package com.fullstack.sales.service01.salesservice.framework.context;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Henry Joseph Calani A.
 */
@Scope("prototype")
@Service
@Transactional
@Retention(RetentionPolicy.RUNTIME)
public @interface ServiceTransactional {
}
