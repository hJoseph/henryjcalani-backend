package com.fullstack.sales.service01.salesservice.controller;

import com.fullstack.sales.service01.salesservice.input.ClientInput;
import com.fullstack.sales.service01.salesservice.model.domain.Client;
import com.fullstack.sales.service01.salesservice.model.domain.Gender;
import com.fullstack.sales.service01.salesservice.response.SuccessRestResponse;
import com.fullstack.sales.service01.salesservice.service.ClientCreateService;
import com.fullstack.sales.service01.salesservice.service.ClientReadByGenderService;
import com.fullstack.sales.service01.salesservice.service.ClientReadByGenderAndEmailService;
import com.fullstack.sales.service01.salesservice.service.DeleteClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Api(
        tags = "client rest",
        description = "Operations over Cliente"
)
@RestController
@RequestMapping("/client")
@RequestScope
public class ClienteController {

    @Autowired
    private ClientCreateService clientCreateService;

    @Autowired
    private DeleteClientService deleteClientService;

    @Autowired
    private ClientReadByGenderService clientReadByGenderService;

    @Autowired
    private ClientReadByGenderAndEmailService clientReadByGenderAndEmailService;

    @ApiOperation(
            value = "Endpoint to create client"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to create client"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Not fount test"
            )
    })

    @RequestMapping(method = RequestMethod.POST)
    public Client createClient(@RequestBody ClientInput input) {
        clientCreateService.setClientInput(input);
        return clientCreateService.save();
    }

    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public List<Client> listClientByGender(@RequestParam("gender") Gender gender){
     clientReadByGenderService.setGender(gender);
     clientReadByGenderService.execute();
     return clientReadByGenderService.getClientList();
    }

   @RequestMapping(value = "/readGender",method = RequestMethod.GET)
    public Client readAccountByEmailAndGender(@RequestParam("email") String email,
                                              @RequestParam("gender") Gender gender) {
        clientReadByGenderAndEmailService.setEmail(email);
        clientReadByGenderAndEmailService.setGender(gender);
        clientReadByGenderAndEmailService.execute();

        return clientReadByGenderAndEmailService.getClient();
    }



    @ApiOperation(value = "Update Client")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Client updateClient(@PathVariable Long id,
                               @RequestParam("gender") Gender gender){
        deleteClientService.setIdClient(id);
        deleteClientService.setGender(gender);
        deleteClientService.execute();
        return deleteClientService.getClient();
    }




}
