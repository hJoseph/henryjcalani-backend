package com.fullstack.sales.service01.salesservice.controller;

/**
 * @Autor Henry Joseph Calani A.
 **/
public final class Constants {

    private Constants() {
    }

    public static class ContactTag {
        public static final String NAME = "contact-controller";

        public static final String DESCRIPTION = "Available operations over contacts";
    }

    public static class DetailTag {
        public static final String NAME = "detail-controller";

        public static final String DESCRIPTION = "Available operations over details";
    }

    public static class BasePath {
        public static final String PUBLIC = "/public";

        public static final String SECURE = "/secure";

        public static final String SYSTEM = "/system";

        public static final String SYSTEM_CONTACTS = SYSTEM + "/client";

        public static final String SECURE_CONTACTS = SECURE + "/client";
    }
}
