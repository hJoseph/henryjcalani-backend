package com.fullstack.sales.service01.salesservice.model.impl;

import com.fullstack.sales.service01.salesservice.model.domain.Person;

/**
 * @Autor Henry Joseph Calani A.
 **/
public final class RootPersonBuilder {

    private RootPersonImpl instance;

    public static RootPersonBuilder getInstance(Person person) {
        return (new RootPersonBuilder()).setPerson(person);
    }

    private RootPersonBuilder() {
        instance = new RootPersonImpl();
    }

    private RootPersonBuilder setPerson(Person person) {
        instance.setPersonId(person.getId());
        instance.setEmail(person.getEmail());
        instance.setFirsName(person.getFirstName());
        instance.setLastName(person.getLastName());
        instance.setDeleted(person.getIsDeleted());


        return this;
    }

    public RootPersonImpl build() {
        return instance;
    }
}
