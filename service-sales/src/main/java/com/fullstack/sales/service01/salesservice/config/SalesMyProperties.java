package com.fullstack.sales.service01.salesservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Configuration
@PropertySource("classpath:/configuration/api-version.properties")
public class SalesMyProperties {

    @Value("${sales.api.version}")
    private String apiVersion;

    public String getApiVersion() {
        return apiVersion;
    }
}