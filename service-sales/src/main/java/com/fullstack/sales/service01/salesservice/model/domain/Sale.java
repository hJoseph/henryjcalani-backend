package com.fullstack.sales.service01.salesservice.model.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/

@Data
@Entity
@Table(name = "Sale_table")
public class Sale {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "numberSale", length = 100, nullable = false)
    private Long numberSale;

    @Column(name = "icreatedDate", length = 100, nullable = false)
    private Date icreatedDate;

}
